import * as React from 'react';

import { withRouter, RouteComponentProps } from "react-router-dom";

import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";

import { UsersRightsTable, UserTable } from '../components';

interface Props extends RouteComponentProps {}

const LoginPageInner = (props: Props) => {

  return (
    <>
      <Card>
          <CardHeader title="Users" />
          <CardContent>
              <UserTable/>
          </CardContent>

            <CardHeader title="User Rights" />
            <CardContent>
              <UsersRightsTable />
            </CardContent>
      </Card>
    </>
  );
};

export const UsersPage = withRouter(LoginPageInner);


