export interface User {
  id: string;
  fName : string;
  lName  : string;
  age: number;
  email: string;
}
