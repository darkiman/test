export interface UserRight {
    id: string;
    name : string;
    enabled  : boolean;
}

