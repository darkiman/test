const TYPES = {
    User: Symbol.for('User'),
    UserService: Symbol.for('UserService'),
    UserRight: Symbol.for('UserRight'),
    DBClient: Symbol.for('DBClient'),
};

export default TYPES;
