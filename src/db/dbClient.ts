const USERS_STORE = 'users';
const USERS_RIGHTS_STORE = 'usersRights';

import * as usersData from './intialData/users.json';
import * as userRightsData from './intialData/userRights.json';

export class DBClient {
    public db: any;
    public usersStore: any;
    public userRightsStore: any;

    private needInitData = false;

    constructor() {
    }

    async initialize() {
        return new Promise((resolve, reject) => {
            let db;
            let dbReq = indexedDB.open('myDatabase', 1);

            dbReq.onupgradeneeded = async (event) => {
                db = event.target['result'];
                this.db = db;
                this.usersStore = db.createObjectStore(USERS_STORE, { autoIncrement: true, keyPath: "id" });
                this.userRightsStore = db.createObjectStore(USERS_RIGHTS_STORE, { autoIncrement: true, keyPath: "id" });
                this.needInitData = true;
            };

            dbReq.onsuccess = async (event) => {
                db = dbReq.result;
                if (this.needInitData) {
                    await this.setInitialData();
                }
                console.log('db initialized');
                this.db = db;
                resolve(db);
            };

            dbReq.onerror = (event) => {
                alert('error opening database ' + event.target['errorCode']);
                reject(event.target);
            };
        })
    }


    private setInitialData = async () => {
        if (usersData) {
            for (let item of usersData['default']) {
                await this.addUser(item);
            }
        }

        if (userRightsData) {
            for (let item of userRightsData['default']) {
                await this.addUserRight(item);
            }
        }
    };

    // Users

    public async addUser(user) {
        return new Promise(async (resolve, reject) => {
            const tx = this.db.transaction(USERS_STORE, 'readwrite');
            const store = await tx.objectStore(USERS_STORE);
            const result = await store.add(user);
            result.onsuccess = (event) => {
                console.log('Success add user', event);
                resolve(event.target.result)
            };
            result.onerror = (error) => {
                console.log('Failed add user', error);
                reject(error);
            };
            return result;
        })
    }

    public async updateUser(user) {
        return new Promise(async (resolve, reject) => {
            const tx = this.db.transaction(USERS_STORE, 'readwrite');
            const store = await tx.objectStore(USERS_STORE);
            const result = await store.put(user);
            result.onsuccess = (event) => {
                console.log('Success update user', event);
                resolve(event.target.result)
            };
            result.onerror = (error) => {
                console.log('Failed update user', error);
                reject(error);
            };
            return result;
        })
    }

    public async deleteUser(user) {
        return new Promise(async (resolve, reject) => {
            const tx = this.db.transaction(USERS_STORE, 'readwrite');
            const store = await tx.objectStore(USERS_STORE);
            const result = await store.delete(user.id, user);
            result.onsuccess = (event) => {
                console.log('Success delete user', event);
                resolve(event.target.result)
            };
            result.onerror = (error) => {
                console.log('Failed delete user', error);
                reject(error);
            };
            return result;
        })
    }

    public async getAllUsers() {
        return new Promise(async (resolve, reject) => {
            const tx = this.db.transaction(USERS_STORE, 'readwrite');
            const store = await tx.objectStore(USERS_STORE);
            const result = await store.getAll();
            result.onsuccess = (event) => {
                console.log('Success get users', event);
                resolve(event.target.result)
            };
            result.onerror = (error) => {
                console.log('Failed get users', error);
                reject(error);
            };
            return result;
        })
    }


    // UserRights

    public async addUserRight(right) {
        return new Promise(async (resolve, reject) => {
            const tx = this.db.transaction(USERS_RIGHTS_STORE, 'readwrite');
            const store = await tx.objectStore(USERS_RIGHTS_STORE);
            const result = await store.add(right);
            result.onsuccess = (event) => {
                console.log('Success add user', event);
                resolve(event.target.result)
            };
            result.onerror = (error) => {
                console.log('Failed add user', error);
                reject(error);
            };
            return result;
        })
    }

    public async updateUserRight(right) {
        return new Promise(async (resolve, reject) => {
            const tx = this.db.transaction(USERS_RIGHTS_STORE, 'readwrite');
            const store = await tx.objectStore(USERS_RIGHTS_STORE);
            const result = await store.put(right);
            result.onsuccess = (event) => {
                console.log('Success update user', event);
                resolve(event.target.result)
            };
            result.onerror = (error) => {
                console.log('Failed update user', error);
                reject(error);
            };
            return result;
        })
    }

    public async deleteUserRight(right) {
        return new Promise(async (resolve, reject) => {
            const tx = this.db.transaction(USERS_RIGHTS_STORE, 'readwrite');
            const store = await tx.objectStore(USERS_RIGHTS_STORE);
            const result = await store.delete(right.id, right);
            result.onsuccess = (event) => {
                console.log('Success delete user right', event);
                resolve(event.target.result)
            };
            result.onerror = (error) => {
                console.log('Failed delete user user', error);
                reject(error);
            };
            return result;
        })
    }

    public async getAllUsersRights() {
        return new Promise(async (resolve, reject) => {
            const tx = this.db.transaction(USERS_RIGHTS_STORE, 'readwrite');
            const store = await tx.objectStore(USERS_RIGHTS_STORE);
            const result = await store.getAll();
            result.onsuccess = (event) => {
                console.log('Success get usersRights', event);
                resolve(event.target.result)
            };
            result.onerror = (error) => {
                console.log('Failed get usersRights', error);
                reject(error);
            };
            return result;
        })
    }
}
