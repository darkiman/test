import React, { createContext, useEffect, useState } from 'react';
import {UsersList} from "../stores/usersList";
import {RightsList} from "../stores/rightsList";
import {DBClient} from "../db/dbClient";

export const StoreContext = createContext<any>({} as any);

const StoreProvider = ({ children }) => {
    const [usersList, setUsersList] = useState(null);
    const [rightsList, setRightsList] = useState(null);

    const getData = async () => {
        const dbClient = new DBClient();
        await dbClient.initialize();

        const allUsers = await dbClient.getAllUsers();
        const allUserRights = await dbClient.getAllUsersRights();

        console.log('allUsers', new UsersList(allUsers as any, dbClient));
        console.log('allUserRights', new RightsList(allUserRights as any, dbClient));

        setUsersList(new UsersList(allUsers as any, dbClient));
        setRightsList(new RightsList(allUserRights as any, dbClient));
    };

    useEffect(() => {
        getData();
    }, [] as any);

    return usersList && rightsList ? (
        <StoreContext.Provider value={{ usersList, rightsList }}>
    {children}
    </StoreContext.Provider>
) : null;
};

export default StoreProvider;
