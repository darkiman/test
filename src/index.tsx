import * as React from "react";
import * as ReactDOM from "react-dom";

import { App } from "./app";
import StoreProvider from "./helpers/store-provider";

ReactDOM.render(
    <StoreProvider> <App /> </StoreProvider>,
    document.getElementById("root")
);
