// import { provide } from 'inversify-binding-decorators';
// import TYPES from '../constant/types';
// import { User } from '../model/user';
// import { inject } from 'inversify';
//
// @provide(TYPES.UserService)
// export class UserService {
//     private dbClient: MongoDBClient;
//
//     constructor(
//         @inject(TYPES.DBClient) dbClient: MongoDBClient
//     ) {
//         this.dbClient = dbClient;
//     }
//
//     public getUsers(): Promise<User[]> {
//         return new Promise<User[]>((resolve, reject) => {
//             this.dbClient.find('user', {}, (error, data: User[]) => {
//                 resolve(data);
//             });
//         });
//     }
//
//     public getUser(id: string): Promise<User> {
//         return new Promise<User>((resolve, reject) => {
//             this.dbClient.findOneById('user', id, (error, data: User) => {
//                 resolve(data);
//             });
//         });
//     }
//
//     public newUser(user: User): Promise<User> {
//         return new Promise<User>((resolve, reject) => {
//             this.dbClient.insert('user', user, (error, data: User) => {
//                 resolve(data);
//             });
//         });
//     }
//
//     public updateUser(id: string, user: User): Promise<User> {
//         return new Promise<User>((resolve, reject) => {
//             this.dbClient.update('user', id, user, (error, data: User) => {
//                 resolve(data);
//             });
//         });
//     }
//
//     public deleteUser(id: string): Promise<any> {
//         return new Promise<any>((resolve, reject) => {
//             this.dbClient.remove('user', id, (error, data: any) => {
//                 resolve(data);
//             });
//         });
//     }
// }
