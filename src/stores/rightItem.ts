import {action, observable} from "mobx";
import {DBClient} from "../db/dbClient";

interface RightData {
    id?: number;
    name:string;
    enabled:boolean;
    userId: string;
}

export default class RightItem {
    @observable id: number = null;
    @observable name: string = '';
    @observable enabled: boolean = false;
    @observable userId: string = '';
    dbClient: DBClient;

    constructor(data: RightData, dbClient:DBClient) {
        this.id = data.id;
        this.name = data.name;
        this.enabled = data.enabled;
        this.userId = data.userId;
        this.dbClient = dbClient;
    }

    @action
    update = async (key: string, value) => {
        this[key] = value;
        await this.dbClient.updateUserRight({
            id: this.id,
            ...this.toObject()
        });
    };

    toObject = () => {
        return { name: this.name, enabled: this.enabled, userId: this.userId };
    }
}
