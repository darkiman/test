import { action, observable, computed } from "mobx";
import RightItem from "./rightItem";
import {DBClient} from "../db/dbClient";

export class RightsList {
    @observable list: RightItem[] = [];
    dbClient: DBClient;

    constructor(rights: any[], dbClient:DBClient) {
        this.dbClient = dbClient;
        if (rights && Array.isArray(rights)) {
            for (let right of rights) {
                this.list.push(new RightItem({id: right.id, name: right.name, enabled: right.enabled, userId: right.userId}, dbClient));
            }
        }
    }

    @action
    addRight = async () => {
        const right = new RightItem({
            id: null,
            name: '',
            enabled: false,
            userId: '',
        }, this.dbClient);
        const dbItemId = await this.dbClient.addUserRight(right.toObject());
        right.id = dbItemId as number;
        this.list.push(right);
    };

    @action
    removeRight = async (right: RightItem) => {
        await this.dbClient.deleteUserRight(right);
        this.list.splice(this.list.indexOf(right), 1);
    };

    @computed
    get rights(): RightItem[] {
        return this.list;
    }
}
