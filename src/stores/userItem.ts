import {action, observable} from "mobx";
import {DBClient} from "../db/dbClient";

interface UserData {
    id?: number;
    fName:string;
    lName:string;
    age:number;
    email: string;
}

export default class UserItem {
    @observable id: number = null;
    @observable fName: string = '';
    @observable lName: string = '';
    @observable age: number = 0;
    @observable email: string = '';
    dbClient: DBClient;

    constructor(data: UserData, dbClient:DBClient) {
        this.id = data.id;
        this.fName = data.fName;
        this.lName = data.lName;
        this.age = data.age;
        this.email = data.email;
        this.dbClient = dbClient;
    }

    @action
    update = async (key: string, value) => {
        this[key] = value;
        await this.dbClient.updateUser({
            id: this.id,
            ...this.toObject()
        });
    };

    toObject = () => {
        return { fName: this.fName, lName: this.lName, age: this.age, email: this.email };
    }
}
