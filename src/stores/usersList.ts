import { action, observable, computed } from "mobx";
import UserItem from "./userItem";
import { DBClient } from "../db/dbClient";

export class UsersList {
    @observable list: UserItem[] = [];
    dbClient: DBClient;

    constructor(users: any[], dbClient:DBClient) {
        this.dbClient = dbClient;
        if (users && Array.isArray(users)) {
            for (let user of users) {
                this.list.push(new UserItem({id: user.id, fName: user.fName, lName: user.lName, age: user.age, email: user.email}, dbClient));
            }
        }
    }

    @action
    addUser = async () => {
        const user = new UserItem({id: null, fName: '', lName: '', age: 0, email: ''}, this.dbClient);
        const dbItemId = await this.dbClient.addUser(user.toObject());
        user.id = dbItemId as number;
        this.list.push(user);
    };

    @action
    removeUser = async (user: UserItem) => {
        await this.dbClient.deleteUser(user);
        this.list.splice(this.list.indexOf(user), 1);
    };

    @computed
    get users(): UserItem[] {
        return this.list;
    }
}
