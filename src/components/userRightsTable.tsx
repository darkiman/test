import * as React from "react";
import {RightItem} from "./rightItem";


import {useObserver} from "mobx-react-lite";
import {useStore} from "../helpers/use-store";
import Button from "@material-ui/core/Button";

interface PropsForm {
}

export const UsersRightsTable = (props: PropsForm) => {
    const store = useStore();

    return useObserver(() => (
        <div>
            {
                store.rightsList.rights.map(right => <RightItem key={`${right.id}`} right={right}/>)
            }

            <Button variant="contained" color="primary" onClick={() => store.rightsList.addRight()}>
                Add Right
            </Button>
        </div>
    ));
};
