import React from 'react';
import RightItemClass from "../stores/rightItem";
import {useStore} from "../helpers/use-store";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import Select from "@material-ui/core/Select"
import FormControl from "@material-ui/core/FormControl";
import InputLabel  from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/styles/makeStyles";
import createStyles from "@material-ui/styles/createStyles";
import {useObserver} from "mobx-react-lite";

interface Props {
    right: RightItemClass;
}

const useControlStyles = makeStyles(theme =>
    createStyles({
        controll: {
           marginRight: '16px'
        },
        select: {
            marginTop: '16px',
            marginRight: '16px',
            minWidth: '80px'
        },
        checkbox: {
            marginRight: '16px',
            marginTop: '16px',
        },
        button: {
            marginTop: '16px',
        }
    })
);

export const RightItem = ({right}: Props) => {
    const store = useStore();
    const classes = useControlStyles();

    const onTextFieldChange = key => e => {
        right.update(key, e.target.value);
    };

    const onCheckBoxChanged = key => e => {
        right.update(key, e.target.checked);
    };

    return useObserver(() => (
        <div>
            <TextField
                label="id"
                className={classes.controll}
                disabled={true}
                margin="normal"
                value={right.id}
                onChange={onTextFieldChange("id")}
            />
            <TextField
                label="Name"
                className={classes.controll}
                margin="normal"
                value={right.name}
                onChange={onTextFieldChange("name")}
            />
            <FormControl className={classes.select}>
                <InputLabel id={'user-id'}>User id</InputLabel>
                <Select
                    labelId={'user-id'}
                    margin="none"
                    value={right.userId}
                    onChange={onTextFieldChange("userId")}
                >
                    {
                        store.usersList.list.map(item => {
                            return  <MenuItem value={item.id} key={item.id}>{item.id}</MenuItem>;
                        })
                    }
                </Select>
            </FormControl>
            <Checkbox
                className={classes.checkbox}
                checked={right.enabled}
                onChange={onCheckBoxChanged("enabled")}
            />

            <Button className={classes.button} variant="contained" color="primary" onClick={() => store.rightsList.removeRight(right)}>
                Delete
            </Button>
        </div>
    ))
};
