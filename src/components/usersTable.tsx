import * as React from "react";
import {UserItem} from "./userItem";

import {useObserver} from "mobx-react-lite";
import {useStore} from "../helpers/use-store";
import Button from "@material-ui/core/Button";

interface PropsForm {
}

export const UserTable = (props: PropsForm) => {
    const store = useStore();

    return useObserver(() => (
        <div>
            {
                store.usersList.users.map(user => <UserItem key={`${user.id}`} user={user}/>)
            }

            <Button variant="contained" color="primary" onClick={() => store.usersList.addUser()}>
                Add User
            </Button>
        </div>
    ));
};
