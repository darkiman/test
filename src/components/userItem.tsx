import React from 'react';
import UserItemClass from "../stores/userItem";
import {useStore} from "../helpers/use-store";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/styles/makeStyles";
import createStyles from "@material-ui/styles/createStyles";
import {useObserver} from "mobx-react-lite";

interface Props {
    user: UserItemClass;
}

const useControlStyles = makeStyles(theme =>
    createStyles({
        controll: {
            marginRight: '16px'
        },
        button: {
            marginTop: '16px',
        }
    })
);

export const UserItem = ({user}: Props) => {
    const store = useStore();
    const classes = useControlStyles();

    const onTextFieldChange = key => e => {
        user.update(key, e.target.value);
    };

    return useObserver(() => (
        <div>
            <TextField
                label="id"
                className={classes.controll}
                margin="normal"
                disabled={true}
                value={user.id}
                onChange={onTextFieldChange("id")}
            />
            <TextField
                label="First name"
                className={classes.controll}
                margin="normal"
                value={user.fName}
                onChange={onTextFieldChange("fName")}
            />
            <TextField
                label="Last name"
                className={classes.controll}
                margin="normal"
                value={user.lName }
                onChange={onTextFieldChange("lName")}
            />
            <TextField
                label="Age"
                className={classes.controll}
                margin="normal"
                value={user.age }
                onChange={onTextFieldChange("age")}
            />
            <TextField
                label="Email"
                className={classes.controll}
                type="email"
                margin="normal"
                value={user.email}
                onChange={onTextFieldChange("email")}
            />
            <Button className={classes.button} variant="contained" color="primary" onClick={() => store.usersList.removeUser(user)}>
                Delete
            </Button>
        </div>
    ))
};
